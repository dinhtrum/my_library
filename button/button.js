import React from 'react'
import PropTypes from 'prop-types'

function Button(props) {
  const { style, className, classNameBtn, classNameContent, title, onClick, icon, type } = props
  console.log('xxx', className)
  return (
    <div className={`button-pos ${className ?? ''}`}>
      <button
        className={`button-default ${classNameBtn ?? ''}`}
        onClick={onClick}
        type={type}
        style={style}
      >
        {icon && icon}
        <p className={`${classNameContent ?? ''}`}>{title}</p>
      </button>
    </div>
  )
}

Button.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  onClick: PropTypes.func,
  icon: PropTypes.element,
}

Button.defaultProps = {
  className: null,
  title: 'Button',
  onClick: () => {},
  icon: null,
}
export default Button
